Project runs on [http://localhost:9001/](http://localhost:9001/)

yarn dev (for running development).

yarn start (for running production).

Technology used:
 - React.js
 - Next.js
 - Express.js

Clone Next.js boilerplate from [https://github.com/MustansirZia/next-express-bootstrap-boilerplate](https://github.com/MustansirZia/next-express-bootstrap-boilerplate) 
 - to decrease development time during initial setup
 - included support for
    - Express.js (for backend api)
    - React-bootstrap (for responsive grid system and components)
    - SCSS (for css styling)
    - Hot reload (for faster preview)
    - Eslint (for cleaner and standardize code)

Data on backend is being populate through random property value and random images, for more real world experience.

Item list on homepage being load by infinite scrolling, made it to load another 2 each time after 2 seconds.

Using media queries at 576px width, as to cater for mobile, tablet, smaller screen laptop, and minimize browser.

Create layout component for housing the header and footer as a common HOC for the pages.

Using reusable hooks and dynamic routes for organize and maintanable components.

All components and pages can be further breakdown to accomodate bigger and more complex projects.

Notes:
 - first time using Next.js, I found it to be quite interesting on the way it manage routes and dynamic routes, and also the ability for SSR, reducing client side loading time.
 - there's definitely a lot more research needs to be done for me to explore all the possibility and benefits using Next.js.

