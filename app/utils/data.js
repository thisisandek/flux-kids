const types = require('./types');

function randomDate() {
  const start = new Date();
  const end = new Date(2020, 12, 12);
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function randomNumber(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

const template = index => ({
  name: `Toy #${index}`,
  type: types[randomNumber(4)],
  photos: new Array(randomNumber(5)).fill(`https://source.unsplash.com/collection/9237614/random?sig=${randomNumber(100)}`),
  availability: randomDate(),
  rating: randomNumber(5) + 1,
});

const data = new Array(15).fill('').reduce((acc, cv, index) => {
  const num = index + 1;
  acc[`item100${num}`] = template(num);
  return acc;
}, {});

module.exports = data;
