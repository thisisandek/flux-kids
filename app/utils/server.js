const dev = process.env.NODE_ENV !== 'production';

const server = dev ? 'http://localhost:9001' : 'https://your_deployment.server.com';

export default server;
