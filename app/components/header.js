import { Navbar, Nav } from 'react-bootstrap';

const Header = () => (
  <div className="header-container">
    <Navbar collapseOnSelect expand="lg" variant="light">
      <Navbar.Brand href="/">
        <div className="logo-container">
          <img alt="" src="/icons/logo.png" />
        </div>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto justify-content-end">
          <Nav.Link href="#link1">Link</Nav.Link>
          <Nav.Link href="#link2">Link</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  </div>
);

export default Header;
