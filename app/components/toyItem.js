import { Carousel } from 'react-bootstrap';
import { isEmpty } from 'ramda';
import Link from 'next/link';
import { ReactSVG } from 'react-svg';

const ToyItem = ({ name, id, type, rating, availability, photos }) => (
  <Link key={id} href={`/toy?id=${id}`} as={`/toy/${id}`}>
    <div className="toy-item-container">
      {photos && !isEmpty(photos) ? (
        <Carousel>
          {photos.map((i, index) => (
            <Carousel.Item key={`${id}_photo_${index + 1}`}>
              <div className="cover-container">
                <img
                  className="d-block w-100 img-carousel"
                  src={i}
                  alt=""
                />
              </div>
            </Carousel.Item>
          ))}
        </Carousel>
      ) : (
        <div className="cover-container">
          <img
            className="d-block w-100 img-carousel placeholder"
            src="/icons/logo.png"
            alt=""
          />
        </div>
      )}

      <div className="details-container">
        <div className="toy-name">
          {name}
        </div>

        <div className="toy-type">
          {type}
        </div>

        <div className="toy-availability">
          {`Available starts: ${new Date(availability).toLocaleDateString('en-US', {
            day: 'numeric',
            month: 'short',
            year: 'numeric',
          })}`}
        </div>

        <div className="toy-rating">
          {new Array(5).fill('').map((i, index) => <ReactSVG key={`${id}_star_${index + 1}`} className="star-icon" src={index < rating ? '/icons/star-full.svg' : '/icons/star-outline.svg'} />)}
        </div>
      </div>
    </div>
  </Link>
);

export default ToyItem;
