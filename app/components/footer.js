
const Footer = () => (
  <div className="footer-container">
    <div className="social-container">
      <img alt="" src="/icons/linkedin-icon.png" />
      <img alt="" src="/icons/facebook-icon.png" />
      <img alt="" src="/icons/twitter-icon.png" />
      <img alt="" src="/icons/instagram-icon.png" />
      <img alt="" src="/icons/medium-icon.png" />
      <img alt="" src="/icons/web-icon.png" />
    </div>
    <div className="img-container">
      <img alt="" src="/icons/footer.png" />
    </div>
  </div>
);

export default Footer;
