import React, { useState } from 'react';
import { Form, Col, Button, Row, Container } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import { isEmpty } from 'ramda';

import DatePickerStyle from '../styles/vendor/react-datepicker.min.css';
// import 'react-datepicker/dist/react-datepicker.css';
import '../styles/vendor/react-datepicker.css';

import * as typeList from '../utils/types';

const EditForm = (props) => {
  const { handleSubmit, data } = props;
  const { name, type, rating, availability, photos } = data;

  const [dateValue, setDateValue] = useState(new Date(availability));
  const [photosLength, setPhotosLength] = useState(photos.length);

  return (
    <Container>
      <style dangerouslySetInnerHTML={{ __html: DatePickerStyle }} />
      <Row>
        <Form onSubmit={handleSubmit}>
          <Form.Row>
            <Form.Group as={Col} md="4" controlId="validationCustom01">
              <Form.Label>Name</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Name"
                defaultValue={name}
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom02">
              <Form.Label>Type</Form.Label>
              <Form.Control
                as="select"
                defaultValue={type}
              >
                {typeList.map(i => <option>{i}</option>)}
              </Form.Control>
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustomUsername">
              <Form.Label>Rating</Form.Label>
              <Form.Control
                as="select"
                defaultValue={rating}
              >
                {new Array(5).fill('').map((i, index) => <option>{index + 1}</option>)}
              </Form.Control>
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group as={Col} md="12" controlId="validationCustom03">
              <Form.Label>Availability</Form.Label>
              <div className="datepicker-container">
                <DatePicker
                  selected={dateValue}
                  onChange={e => setDateValue(e)}
                />
              </div>
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group as={Col} md="12" controlId="photos-id">
              <Form.Label>Photos</Form.Label>
              {new Array(photosLength).fill('').map((i, index) => (
                <Form.Control
                  type="text"
                  placeholder="Url"
                  defaultValue={!isEmpty(photos) && photos[index]}
                />
              ))}
              <div className="button-container">
                <Button variant="info" className="add-button" onClick={() => setPhotosLength(photosLength + 1)}>ADD FIELD</Button>
                <Button variant="danger" className="remove-button" onClick={() => setPhotosLength(photosLength - 1)}>REMOVE FIELD</Button>
              </div>
            </Form.Group>
          </Form.Row>
          <Button variant="success" className="submit-button" type="submit">S U B M I T</Button>
        </Form>
      </Row>
    </Container>
  );
};

export default EditForm;
