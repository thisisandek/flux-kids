import PropTypes from 'prop-types';
import Theme from './Theme';
import Header from './header';
import Footer from './footer';
import indexStyle from '../styles/index.scss';

const Layout = ({ children }) => (
  <div className="layout-container">
    <style dangerouslySetInnerHTML={{ __html: indexStyle }} />
    <Theme>
      <Header />
      {children}
      <Footer />
    </Theme>
  </div>
);

Layout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]).isRequired,
};

export default Layout;
