const { isEmpty } = require('ramda');
const isImageUrl = require('is-image-url');
const data = require('../utils/data');

exports.getSingleItem = (req, res) => {
  const { id } = req.params;
  const item = data[id];

  if (item) {
    return res.status(200).json(item).send('success get item');
  }
  return res.status(100).json({}).send('no item associated with id');
};

exports.getAllData = (req, res) => {
  return (data && !isEmpty(data)
    && res.status(200).json(data).send('success get all data'))
    || res.status(100).json({}).send('no data available');
};

exports.editItem = (req, res) => {
  const { name, type, rating, availability, photos } = req.body;

  const condition = !name || !type || !rating || !availability || isEmpty(photos) || photos.some(i => !isImageUrl(i));

  if (condition) {
    return res.end(res.writeHead(300, 'Some input is/are incorrect'));
  }
  return res.end(res.writeHead(200, 'All data is correct'));
};
