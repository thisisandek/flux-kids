import React, { useState, useEffect } from 'react';
import Router from 'next/router';
import fetch from 'isomorphic-unfetch';
import { isEmpty } from 'ramda';

import server from '../utils/server';
import EditPageStyle from '../styles/edit.scss';
import Layout from '../components/layout';
import EditForm from '../components/editForm';

const submitApi = (setValidate, event) => {
  event.preventDefault();
  const form = event.currentTarget;
  const photos = Object.values({ ...form }).reduce((acc, i) => {
    if (i.id && i.id === 'photos-id') acc.push(i.value);
    return acc;
  }, []);

  const formData = {
    name: form[0].value,
    type: form[1].value,
    rating: form[2].value,
    availability: new Date(form[3].value).toISOString(),
    photos,
  };

  fetch('/edit-item', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(formData)
  })
    .then(response => setValidate({ text: response.statusText, code: response.status }))
    // .then(data => console.log(data))
    .catch(error => console.log(error));
};

const EditPage = (props) => {
  const { data, id } = props;

  const [validate, setValidate] = useState(null);

  const handleSubmit = (event) => {
    event.preventDefault();
    submitApi(setValidate, event);
  };

  useEffect(() => {
    if (validate) {
      setTimeout(() => setValidate(null), 3000);
    }
  }, [validate]);

  if (isEmpty(data)) Router.push('/');

  return (
    <Layout>
      <div className="edit-page-container">
        <style dangerouslySetInnerHTML={{ __html: EditPageStyle }} />
        <EditForm handleSubmit={handleSubmit} data={{ ...data, id }} />
        {validate && (
          <div className={`validate-text code-${validate.code}`}>
            {validate.text}
          </div>
        )}
      </div>
    </Layout>
  );
};

EditPage.getInitialProps = async (props) => {
  const id = (props.query && props.query.id) || (props.req && props.req.params && props.req.params.id) || null;

  const res = await fetch(`${server}/single-item/${id}`);
  const json = await res.json();

  return { data: json, id };
};

export default EditPage;
