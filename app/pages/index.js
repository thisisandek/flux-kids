import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Container, Row, Col, Spinner } from 'react-bootstrap';
import fetch from 'isomorphic-unfetch';
import { isEmpty } from 'ramda';
import InfiniteScroll from 'react-infinite-scroller';

import indexStyle from '../styles/index.scss';
import Layout from '../components/layout';
import ToyItem from '../components/toyItem';


const Index = () => {
  const [dataList, setDataList] = useState(null);
  const [limit, setLimit] = useState(6);

  const getAllData = () => fetch('/all-data')
    .then((res) => res.json())
    .then(data => setDataList(data));

  useEffect(() => {
    getAllData();
  }, []);

  return (
    <Layout>
      <div className="index-container">
        <style dangerouslySetInnerHTML={{ __html: indexStyle }} />
        <Container>
          {dataList === null && (
            <Row>
              <Col>
                <div className="loading-container">
                  <Spinner animation="border" size="lg" />
                  <div>L O A D I N G</div>
                </div>
              </Col>
            </Row>
          )}

          {dataList && !isEmpty(dataList) && (
            <InfiniteScroll
              pageStart={0}
              loadMore={() => setTimeout(() => setLimit(limit + 2), 2000)}
              hasMore={limit < Object.keys(dataList).length}
              loader={<Row key="row-loading"><Col key="col-loading"><div className="scroll-loading" key={0}>Loading...</div></Col></Row>}
            >
              <Row key="row-inside">
                {Object.keys(dataList).map((id, index) => {
                  const item = dataList[id];
                  if (index < limit) {
                    return (
                      <Col key={id} xs={12} sm={6} md={6} lg={4}>
                        <ToyItem key={id} id={id} {...item} />
                      </Col>
                    );
                  }
                  return null;
                })}
              </Row>
            </InfiniteScroll>
          )}
        </Container>
      </div>
    </Layout>
  );
};

export default Index;
