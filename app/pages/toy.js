import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import { Container, Row, Col, Button, Carousel } from 'react-bootstrap';
import fetch from 'isomorphic-unfetch';
import { isEmpty } from 'ramda';
import { ReactSVG } from 'react-svg';

import server from '../utils/server';
import toyPageStyle from '../styles/toy.scss';
import Layout from '../components/layout';

const ToyPage = (props) => {
  const { data, id, description } = props;

  if (isEmpty(data)) Router.push('/');

  const { name, photos, type, availability, rating } = data;

  return (
    <Layout>
      <div className="toy-page-container">
        <style dangerouslySetInnerHTML={{ __html: toyPageStyle }} />
        <div className="carousel-container">
          {photos && !isEmpty(photos) ? (
            <Carousel>
              {photos.map((i, index) => (
                <Carousel.Item key={`${id}_photo_${index + 1}`}>
                  <div className="cover-container" onClick={() => alert('open image full')}>
                    <img
                      className="d-block w-100 img-carousel"
                      src={i}
                      alt=""
                    />
                  </div>
                </Carousel.Item>
              ))}
            </Carousel>
          ) : (
            <div className="cover-container">
              <img
                className="d-block w-100 img-carousel placeholder"
                src="/icons/logo.png"
                alt=""
              />
            </div>
          )}
        </div>

        <Container>
          <Row>
            <Col>
              <div className="toy-name">
                {name}
              </div>
            </Col>
            <Col xs={12} sm={4}>
              <div className="toy-rating">
                {new Array(5).fill('').map((i, index) => <ReactSVG key={`${id}_star_${index + 1}`} className="star-icon" src={index < rating ? '/icons/star-full.svg' : '/icons/star-outline.svg'} />)}
              </div>
            </Col>
          </Row>

          <Row>
            <Col>
              <div className="toy-type">
                {type}
              </div>
            </Col>
          </Row>

          <Row>
            <Col>
              <div className="toy-availability">
                {`Available starts: ${new Date(availability).toLocaleDateString('en-US', {
                  day: 'numeric',
                  month: 'short',
                  year: 'numeric',
                })}`}
              </div>
            </Col>
          </Row>

          <Row>
            <Col>
              <div className="toy-description">
                <span dangerouslySetInnerHTML={{ __html: description }} />
              </div>
            </Col>
          </Row>
        </Container>

        <div className="floating-container">
          <Link href={`/edit?id=${id}`} as={`/edit/${id}`}>
            <Button className="edit-button">
              E D I T
            </Button>
          </Link>
        </div>
      </div>
    </Layout>
  );
};

ToyPage.getInitialProps = async (props) => {
  const id = (props.query && props.query.id) || (props.req && props.req.params && props.req.params.id) || null;

  const res = await fetch(`${server}/single-item/${id}`);
  const json = await res.json();

  const sampleTextRes = await fetch('http://www.randomtext.me/api/lorem/p-1/25-48');
  const sampleText = (await sampleTextRes.json()).text_out;
  return { data: json, id, description: sampleText };
};

export default ToyPage;
